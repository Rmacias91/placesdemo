# PlacesDemo

Use places API to populate a map and list for favorite restaurants. 

# Current Status

1. App just loads default map visually
2. Removed code to request api service for restaurants, due to an unforseen bug related to locationServices and the registerForActivityResult. I think it was triggering this callback.

3. Api callback was working as expected for nearby searches.

# Future feature work

1. Remove that delay and come up with a better design dealing with location permissions.
2. Add map markers with detailed info of the restaurants.
3. Add a fragment with a recyclerList that takes restaurants in its fragment arguments.

# Future Improvements
1. Add injection, especially for retrofit client to be a singleton and testing to be easier.
2. Come up with a better MVVM flow, or switch over to MVI completley.
3. Fragment could use a baseFragment Class to clean up subscriptions with the autoDisposer class.

# Reflecting
Sorry I didn't finish the project, it took longer than I was expecting. I learned that in my current team, the app is very abstracted from Android, and my knowledge is lacking in google maps and location services. Not sure I would be a good canidate since my experience here is lacking. I also need to clean up my Rx logic and organize the code better. I also didn't have time to use dagger. In retrospect, I should have ignored the location services, used mock latitude and longitude, and finished the ui. The logic around location services ate most of my time and implemtning mvvm in general. I should have taken a step back and thought out a better way to implement it. I think it was a good eye opener on my current skills and look forward to making my own app in the near future on my free time. Thanks for the oppurtunity!! 
