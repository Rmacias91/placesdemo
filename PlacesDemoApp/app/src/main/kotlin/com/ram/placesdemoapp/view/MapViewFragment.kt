package com.ram.placesdemoapp.view

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.ram.placesdemoapp.AutoDisposable
import com.ram.placesdemoapp.R
import com.ram.placesdemoapp.viewModel.MapViewModel
import com.ram.placesdemoapp.viewModel.MapViewState
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers

private const val MAP_VIEW_BUNDLE_KEY = "mapViewBundleKey"

class MapViewFragment : Fragment(), OnMapReadyCallback {

    // TODO Make this view binding
    private var mapView: MapView? = null
    private var googleMap: GoogleMap? = null
    private val mapViewModel: MapViewModel by viewModels()
    private val permissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            // mapViewModel.updateLocationPermissions(it)
            // TODO BUG I believe, The locationRepository is triggering this callback,
            // I believe it's from the service triggering it.
        }

    // We need to make a base rxjava fragment class for cleaning up
    private val autoDisposer = AutoDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        autoDisposer.bindTo(this.lifecycle)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_map_view, container, false)
        mapView = view?.findViewById(R.id.mapview)
        mapView?.onCreate(savedInstanceState?.getBundle(MAP_VIEW_BUNDLE_KEY))
        mapView?.getMapAsync(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        autoDisposer.addDisposable(
            mapViewModel.observeViewStateEvents()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(::render)
        )
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        // We can customize our map here
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        var mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY)
        if (mapViewBundle == null) {
            mapViewBundle = Bundle()
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle)
            mapView?.onSaveInstanceState(mapViewBundle)
        } else {
            mapView?.onSaveInstanceState(mapViewBundle)
        }
    }

    private fun render(viewState: MapViewState) {
        when (viewState.state) {
            MapViewState.State.Loading -> {
            }
            is MapViewState.State.Loaded -> loadData(viewState, viewState.state)
            MapViewState.State.Error -> {
            }
        }
        if (!viewState.locationPermissionGranted) {
            requestLocationPermission()
        }
    }

    private fun loadData(viewState: MapViewState, data: MapViewState.State.Loaded) {
        if (viewState.locationPermissionGranted) {
            updateLocation(viewState.currentLocation)
            //data.restaurants Load map markers
        }
    }

    private fun updateLocation(currentLocation: Pair<Double, Double>) {
        val point =
            CameraUpdateFactory.newLatLng(LatLng(currentLocation.first, currentLocation.second))
        googleMap?.moveCamera(point)
    }

    private fun placeMarkers() {

    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onPause() {
        mapView?.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mapView?.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    private fun requestLocationPermission() {
        permissionLauncher.launch(Manifest.permission.ACCESS_COARSE_LOCATION)
    }
}
