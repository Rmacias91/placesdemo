package com.ram.placesdemoapp.helpers

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.google.android.gms.location.* // Not good practice
import io.reactivex.rxjava3.core.Single

private const val LOCATION_INTERVAL = 6000L
private const val LOCATION_FASTEST_INTERVAL = 5000L

/**
 * LocationRepository used to start location services and retrieve latest location
 *
 *
 **/
class LocationRepository(private val application: Application) {
    private val locationRequest = LocationRequest.create()
        .setInterval(LOCATION_INTERVAL)
        .setFastestInterval(LOCATION_FASTEST_INTERVAL)
        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(application)
    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(p0: com.google.android.gms.location.LocationResult) {
            super.onLocationResult(p0)
        }

        override fun onLocationAvailability(p0: LocationAvailability) {
            super.onLocationAvailability(p0)
        }
    }

    @SuppressLint("MissingPermission")
    fun startLocationService() {
        if (ContextCompat.checkSelfPermission(
                application.applicationContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
        }
    }

    fun removeLocationServices() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    @SuppressLint("MissingPermission")
    fun retrieveLastLocation(): Single<LocationResult> =
        if (ContextCompat.checkSelfPermission(
                application.applicationContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            Single.create { emitter ->
                val task = fusedLocationClient.lastLocation
                task.addOnSuccessListener {
                    emitter.onSuccess(
                        LocationResult.Location(
                            it.latitude,
                            it.longitude
                        )
                    )
                }
                task.addOnFailureListener { emitter.onError(it) }
            }
        } else {
            Single.just(LocationResult.PermissionNeeded)
        }
}

sealed class LocationResult {

    data class Location(val lat: Double, val lng: Double) : LocationResult()

    object PermissionNeeded : LocationResult()
}
