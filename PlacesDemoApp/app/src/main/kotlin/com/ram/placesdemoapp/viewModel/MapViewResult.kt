package com.ram.placesdemoapp.viewModel

import com.ram.placesdemoapp.model.Restaurant

sealed class MapViewResult {
    data class LoadContent(
        val currentLocation: Pair<Double, Double>,
        val restaurants: List<Restaurant>
    ) : MapViewResult()

    data class Error(val message: String) : MapViewResult()
    data class LocationPermissionUpdated(val granted: Boolean) : MapViewResult()
    object Loading : MapViewResult()
}
