package com.ram.placesdemoapp.network

import io.reactivex.rxjava3.core.Single

internal typealias Location = Pair<Double, Double>

class PlacesApiService {
    // We gotta inject that singleton!!
    private val retrofitClient = RetrofitClient()

    fun nearByRestaurants(radius: Int, location: Location) =
        nearByPlaces(PlaceType.RESTAURANT.value, radius, location, PlaceType.RESTAURANT)

    private fun nearByPlaces(
        keyword: String,
        radius: Int,
        location: Location,
        type: PlaceType
    ): Single<NearbyPlace> =
        retrofitClient.nearbyPlaces.nearByPlace(
            keyword,
            radius,
            locationString(location),
            type.value
        )

    private fun locationString(location: Location): String =
        "${location.first},${location.second}"
}
