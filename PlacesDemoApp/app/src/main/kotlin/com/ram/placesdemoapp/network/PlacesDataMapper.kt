package com.ram.placesdemoapp.network

import com.ram.placesdemoapp.model.Restaurant
import com.ram.placesdemoapp.model.RestaurantState

internal fun mapToRestaurantResponse(placeResponse: NearbyPlace): RestaurantState =
    when (placeResponse.status) {
        PlaceSearchStatus.OK -> mapToRestaurants(placeResponse)
        PlaceSearchStatus.ZERO_RESULTS -> RestaurantState.Restaurants(emptyList())
        else -> RestaurantState.Error
    }

private fun mapToRestaurants(placeResponse: NearbyPlace): RestaurantState.Restaurants =
    RestaurantState.Restaurants(
        placeResponse.results.mapNotNull {
            if (!it.name.isNullOrEmpty() && it.geometry !== null) {
                Restaurant(
                    it.name,
                    it.geometry.location.let { location -> location.lat to location.lng })
            } else {
                null
            }
        }
    )
