package com.ram.placesdemoapp.network

enum class PlaceType(val value: String) {
    RESTAURANT("restaurant")
}
