package com.ram.placesdemoapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ram.placesdemoapp.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Do I need a tag/id ? This question specifics
        // Will be more clear when we start navigation. Should I look into nav libraries?
        if (savedInstanceState == null) {
            val mapViewFragment = MapViewFragment()
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, mapViewFragment)
                .commit()
        }
    }
}
