package com.ram.placesdemoapp.network

import com.ram.placesdemoapp.model.RestaurantState
import io.reactivex.rxjava3.core.Single

private const val DEFAULT_RADIUS = 3000

class PlacesRepository {
    // Again inject here
    private val placesRepositoryLPlacesApi = PlacesApiService()

    fun retrieveNearByRestaurants(location: Location): Single<RestaurantState> =
        placesRepositoryLPlacesApi.nearByRestaurants(DEFAULT_RADIUS, location)
            .map { mapToRestaurantResponse(it) }
}
