package com.ram.placesdemoapp.network

import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PlacesApi {

    @GET("nearbysearch/json")
    fun nearByPlace(
        @Query("keyword") keyword: String,
        @Query("radius") radius: Int,
        @Query("location") location: String,
        @Query("type") type: String
    ): Single<NearbyPlace>
}
