package com.ram.placesdemoapp.network

import com.ram.placesdemoapp.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

private const val BASE_URL = "https://maps.googleapis.com/maps/api/place/"

// We need this all to be a singleton
class RetrofitClient(
    okHttpClient: OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(OkHttpQueryInterceptor())
        .addInterceptor(
            HttpLoggingInterceptor().apply {
                if (BuildConfig.DEBUG) {
                    setLevel(HttpLoggingInterceptor.Level.BODY)
                } else {
                    setLevel(HttpLoggingInterceptor.Level.NONE)
                }
            }
        )
        .build(),

    retrofitClient: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .build()
) {
    // We can provide service implementation instead of making values in this class if we use dagger
    val nearbyPlaces: PlacesApi = retrofitClient.create(PlacesApi::class.java)
}
