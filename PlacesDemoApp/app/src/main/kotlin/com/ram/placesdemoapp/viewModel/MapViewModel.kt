package com.ram.placesdemoapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.ram.placesdemoapp.helpers.LocationRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject

class MapViewModel(application: Application) : AndroidViewModel(application) {
    private val compositeDisposable = CompositeDisposable()
    private val resultFactory: MapResultFactory = MapResultFactory()
    private val locationRepository = LocationRepository(application)
    private var viewState: MapViewState = MapViewState(MapViewState.State.Loading)
    private val viewStateSubject: BehaviorSubject<MapViewState> =
        BehaviorSubject.createDefault(MapViewState(state = MapViewState.State.Loading))

    init {
        locationRepository.startLocationService()
        loadRestaurants()
    }

    fun observeViewStateEvents(): Observable<MapViewState> = viewStateSubject.hide()

    fun updateLocationPermissions(granted: Boolean) {
        if (granted) {
            locationRepository.startLocationService()
            loadRestaurants()
        } else {
            updateViewState(MapViewResult.LocationPermissionUpdated(false))
        }
    }

    private fun loadRestaurants() {
        compositeDisposable.add(
            locationRepository.retrieveLastLocation().toObservable()
                .delay(
                    1,
                    java.util.concurrent.TimeUnit.SECONDS
                ) // TODO wait for location to updated NEVER add delays This is horrible code here
                // Need to revisit the logic here and use a completeable?
                .flatMap { resultFactory.loadRestaurants(it) }
                .map { updateViewState(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        )
    }

    private fun updateViewState(result: MapViewResult) {
        viewState = when (result) {
            is MapViewResult.LoadContent -> viewState.copy(
                state = MapViewState.State.Loaded(result.restaurants),
                currentLocation = result.currentLocation
            )
            is MapViewResult.Error -> viewState.copy(state = MapViewState.State.Error)
            MapViewResult.Loading -> viewState.copy(state = MapViewState.State.Loading)
            is MapViewResult.LocationPermissionUpdated -> viewState.copy(locationPermissionGranted = result.granted)
        }
        viewStateSubject.onNext(viewState)
    }

    override fun onCleared() {
        super.onCleared()
        locationRepository.removeLocationServices()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }
    }
}
