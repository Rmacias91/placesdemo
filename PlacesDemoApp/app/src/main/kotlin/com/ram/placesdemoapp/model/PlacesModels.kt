package com.ram.placesdemoapp.model

sealed class RestaurantState {

    data class Restaurants(val restaurants: List<Restaurant>) : RestaurantState()

    object Error : RestaurantState()
}

data class Restaurant(
    val name: String,
    val location: Pair<Double, Double>
)
