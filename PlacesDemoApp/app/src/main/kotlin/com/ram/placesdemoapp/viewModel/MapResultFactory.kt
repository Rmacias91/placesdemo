package com.ram.placesdemoapp.viewModel

import com.ram.placesdemoapp.helpers.LocationResult
import com.ram.placesdemoapp.model.RestaurantState
import com.ram.placesdemoapp.network.PlacesRepository
import io.reactivex.rxjava3.core.Observable

// Intended to move logic of results outside of ViewModel
// But I needed to do RxFlows and subscriptions here I believe.
class MapResultFactory {
    private val repository = PlacesRepository()

    fun loadRestaurants(locationResult: LocationResult): Observable<MapViewResult> =
        when (locationResult) {
            LocationResult.PermissionNeeded -> Observable.just(
                MapViewResult.LocationPermissionUpdated(
                    false
                )
            )
            is LocationResult.Location -> retrieveNearByRestaurants(locationResult)
        }

    private fun retrieveNearByRestaurants(location: LocationResult.Location) =
        repository.retrieveNearByRestaurants(location.lat to location.lng).map {
            when (it) {
                is RestaurantState.Restaurants -> MapViewResult.LoadContent(
                    location.lat to location.lng,
                    it.restaurants
                )
                is RestaurantState.Error -> MapViewResult.Error(it.toString())
            }
        }
            .toObservable()
            .onErrorResumeNext {
                Observable.just(
                    MapViewResult.Error(
                        it.message ?: "unknown error"
                    )
                )
            }
}
