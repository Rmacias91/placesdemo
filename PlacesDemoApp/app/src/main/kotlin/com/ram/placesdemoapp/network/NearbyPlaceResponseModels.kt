package com.ram.placesdemoapp.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NearbyPlace(
    @Json(name = "html_attributions")
    val htmlAttributes: List<String>,
    val results: List<Place>,
    val status: PlaceSearchStatus,
    @Json(name = "error_message")
    val errorMessage: String?,
    @Json(name = "info_messages")
    val infoMessages: List<String>?,
    @Json(name = "next_page_token")
    val nextPageToken: String?
)

@JsonClass(generateAdapter = true)
data class Place(
    // TODO Not adding all fields to the response. Room to add more for detail screen enhancements
    // https://developers.google.com/maps/documentation/places/web-service/search-nearby#Place
    @Json(name = "formatted_address")
    val address: String?,
    @Json(name = "business_status")
    val businessStatus: String?,
    @Json(name = "formatted_phone_number")
    val phoneNumber: String?,
    val geometry: Geometry?,
    val icon: String?,
    @Json(name = "icon_background_color")
    val iconBackgroundColor: String?,
    val name: String?,
    val photos: List<PlacePhoto>?,
    @Json(name = "place_id")
    val placeId: String?
)

@JsonClass(generateAdapter = true)
data class Geometry(
    val location: LatLngLiteral,
    val viewPort: Bounds?
)

@JsonClass(generateAdapter = true)
data class LatLngLiteral(
    val lat: Double,
    val lng: Double
)

@JsonClass(generateAdapter = true)
data class Bounds(
    val northeast: Int,
    val southwest: Int
)

@JsonClass(generateAdapter = true)
data class PlacePhoto(
    val height: Int,
    val width: Int,
    @Json(name = "html_attributions")
    val htmlAttributions: List<String>,
    val photoReference: String?
)

enum class PlaceSearchStatus {
    OK,
    ZERO_RESULTS,
    INVALID_REQUEST,
    OVER_QUERY_LIMIT,
    REQUEST_DENIED,
    UNKNOWN_ERROR
}
