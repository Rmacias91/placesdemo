package com.ram.placesdemoapp.network

import com.ram.placesdemoapp.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

private const val PLACES_NAME = "key"

class OkHttpQueryInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val url = request.url.newBuilder()
            .addQueryParameter(PLACES_NAME, BuildConfig.PLACES_API_KEY)
            .build()
        val newRequest = request.newBuilder().url(url).build()
        return chain.proceed(newRequest)
    }
}