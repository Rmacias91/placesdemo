package com.ram.placesdemoapp

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import java.lang.IllegalStateException

class AutoDisposable : LifecycleObserver {

    lateinit var compositeDisposables: CompositeDisposable

    fun bindTo(lifeCycle: Lifecycle) {
        lifeCycle.addObserver(this)
        compositeDisposables = CompositeDisposable()
    }

    fun addDisposable(disposable: Disposable) {
        if (::compositeDisposables.isInitialized) {
            compositeDisposables.add(disposable)
        } else {
            throw IllegalStateException("This object needs to be bound to a lifeCycle first")
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onDestroy() {
        // We want to clear for fragments since they are retained in backstacks
        // But for activites is it better to dispose vs clear?
        compositeDisposables.clear()
    }
}
