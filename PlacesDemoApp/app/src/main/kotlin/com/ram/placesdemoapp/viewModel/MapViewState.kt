package com.ram.placesdemoapp.viewModel

import com.ram.placesdemoapp.model.Restaurant

data class MapViewState(
    val state: State,
    val locationPermissionGranted: Boolean = false,
    val currentLocation: Pair<Double, Double> = 0.0 to 0.0
) {
    sealed class State {
        object Loading : State()
        data class Loaded(
            val restaurants: List<Restaurant>
        ) : State()

        object Error : State()
    }
}
